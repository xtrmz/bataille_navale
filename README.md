* travis: [![Build Status](https://travis-ci.org/lmazardo/bataille_navale.svg?branch=master)](https://travis-ci.org/lmazardo/bataille_navale)
* gitlab: [![pipeline status](https://gitlab.com/xtrmz/bataille_navale/badges/master/pipeline.svg)](https://gitlab.com/xtrmz/bataille_navale/commits/master)

Git
===
Quelques pointeurs sur [GIT](https://gitlab.com/unix_tips_and_tricks/commandline/blob/master/git/README-fr.md)

Maven
=====
Utilisation du plugin *maven-jar-plugin* afin de créer un exécutable (jar)

IDE
===
 * [Intellij IDEA refcard] (https://www.jetbrains.com/idea/docs/IntelliJIDEA_ReferenceCard.pdf)
 * [Eclipse shortcuts] (https://github.com/pellaton/eclipse-cheatsheet/blob/master/eclipse4.3/eclipse-shortcuts-4.3.0.pdf?raw=true)
 * [NetBeans shortcuts] (https://netbeans.org/project_downloads/usersguide/shortcuts-80.pdf)
 
